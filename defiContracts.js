const decoder = require('./decoder')
const writeFile = require('./writeFile')
const contractInstances = require('./contractInstances')
const web3 = require('./web3')

const eventFields = [
  'blockNumber',
  'blockHash',
  'blockTimestamp',
  'contractAddress',
  'contractName',
  'trxHash',
  'eventName',
  'eventArgs'
]

const formatEventArgs = eventArgs => eventArgs.map(arg => `${arg.name}:${arg.value}`).join('|')

const formatEvent = event => {
  const decoded = decoder.decodeLogs([event])[0]
  const eventName = decoded ? decoded.name : 'unknown'
  const eventArgs = decoded ? formatEventArgs(decoded.events) : 'unknown'
  return {
    eventName,
    eventArgs
  }
}

const eventToCsv = (contractName, event) => {
  const { eventName, eventArgs } = formatEvent(event)

  return [
    event.blockNumber,
    event.blockHash,
    event.address,
    contractName,
    event.transactionHash,
    eventName,
    eventArgs

  ].join(',')
}

const generateLogsForContract = async (contractName, contractInstance, fromBlock, toBlock) => {
  const logFilter = {
    fromBlock,
    toBlock,
    address: contractInstance.address
  }
  
  // fetch logs from the node
  const logs = await web3.infuraProvider.getLogs(logFilter)

  // format the logs returned by the node provider into a csv log suitable format
  const csvLogs = logs.map(eventToCsv.bind(this, contractName))

  // add the field names as the first line
  csvLogs.unshift(eventFields)

  const fileName = `${contractName}_${contractInstance.address}_${fromBlock}_${toBlock}.csv`

  // write out the logs to a file
  writeFile(fileName, csvLogs.join('\n'))
}



const generateLogFiles = async () => {
  // number of blocks to fetch in each request
  const batchSize = 1000

  // number of requests to make per contract
  let batchCount = 40

  // block to start on and request older blocks after the initial request
  let endBlock = 8209460//8249500//8289540//8329580//8369620//8409660//8439690//8469720//8499759//8501262//8511281

  while(batchCount) {
    const startBlock = endBlock - batchSize
    console.log(`requesting blocks ${startBlock} to ${endBlock}, ${batchCount} to go`)
    await Promise.all(contractInstances.map(
      async ci =>  await generateLogsForContract(ci.name, ci.instance, startBlock, endBlock )
      )
    )
    endBlock = startBlock - 1
    batchCount--
  }
  console.log('finished')
  //process.exit()

}


module.exports.generateLogFiles = generateLogFiles
