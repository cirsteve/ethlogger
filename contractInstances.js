const daiAbi = require('./abis/dai')
const usdcAbi = require('./abis/usdc')
const compoundErc20Abi = require('./abis/compoundErc20')
const compoundEthAbi = require('./abis/compoundEth')
const uniswapErc20Abi = require('./abis/uniswapErc20')
const web3 = require('./web3')
const ethers = require('ethers')

const contractsOfInterest = [
  {
    address: '0x89d24A6b4CcB1B6fAA2625fE562bDD9a23260359',
    abiType: 'dai',
    name: 'daiToken'
  },
  {
    address: '0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48',
    abiType: 'usdc',
    name: 'usdcToken'
  },
  // PAX: '0x8e870d67f660d95d5be530380d0ec0bd388289e1',
  // TUSD: '0x0000000000085d4780B73119b644AE5ecd22b376'
  // TETHER: '0xdac17f958d2ee523a2206206994597c13d831ec7'
  {
    address: '0xF5DCe57282A584D2746FaF1593d3121Fcac444dC',
    abiType: 'compoundErc20',
    name: 'compoundDai'
  },
  {
    address: '0x4Ddc2D193948926D02f9B1fE9e1daa0718270ED5',
    abiType: 'compoundEth',
    name: 'compoundEth'
  },
  {
    address: '0x39AA39c021dfbaE8faC545936693aC917d5E7563',
    abiType: 'compoundErc20',
    name: 'compoundUsdc'
  },  
  {
    address: '0x09cabEC1eAd1c0Ba254B09efb3EE13841712bE14',
    abiType: 'uniswapErc20',
    name: 'uniswapDai'
  }, 
  // UNISWAP_ETH: {
  //   address: '0x4740C758859D4651061CC9CDEFdBa92BDc3a845d',
  //  abiType: 'uniswapErc20',
  //  name: 'daiToken'
  // },
  {
    address: '0x97deC872013f6B5fB443861090ad931542878126',
    abiType: 'uniswapErc20',
    name: 'uniswapUsdc'
  }
]

const abis = {
    dai: daiAbi,
    usdc: usdcAbi,
    compoundErc20: compoundErc20Abi,
    compoundEth: compoundEthAbi,
    uniswapErc20: uniswapErc20Abi
}

const contractInstances = (() => contractsOfInterest.map((acc, k) => {
  const contract = contractsOfInterest[k]
  return {
    ...contract,
    instance: new ethers.Contract(contract.address,abis[contract.abiType], web3.infuraProvider)
  }
}))()

module.exports = contractInstances