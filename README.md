# EthLogger

A simple POC script to fetch logs a provider, format the logs, and write them to a file. Currently works against infura node.

Contracts of interest are specified in the contractInstances.js file along with the abi they implement. To run set the
batchSize, batchCount, and endBlock variables in the defiContracts.js file and execute the index file 
node index.js
