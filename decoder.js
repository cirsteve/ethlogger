const daiAbi = require('./abis/dai')
const usdcAbi = require('./abis/usdc')
const compoundErc20Abi = require('./abis/compoundErc20')
const compoundEthAbi = require('./abis/compoundEth')
const uniswapErc20Abi = require('./abis/uniswapErc20')
const abiDecoder = require('abi-decoder')
 
const decoder = (() => {
    abiDecoder.addABI(daiAbi)
    abiDecoder.addABI(usdcAbi)
    abiDecoder.addABI(compoundErc20Abi)
    abiDecoder.addABI(compoundEthAbi)
    abiDecoder.addABI(uniswapErc20Abi)
    return abiDecoder
})()

module.exports = decoder