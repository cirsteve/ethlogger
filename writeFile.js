const fs = require('fs');

const writeFile = (fileName, content) => {
    fs.writeFile(`ethLogs/${fileName}`, content, err => console.log(fileName, err))
}

module.exports = writeFile